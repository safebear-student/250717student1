package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    WebDriver driver;

   @FindBy(linkText = "Login")
    WebElement loginLink1;

   @FindBy(linkText = "Go to the frames »")
   WebElement gotoFrames;

   // @FindBy(xpath = "//*[@id="navbar"]/ul/li[2]/a")
  //  WebElement loginlink2

            public WelcomePage(WebDriver driver)
            {
                this.driver = driver;
                PageFactory.initElements(driver,this);
            }
            public boolean checkCorrectPage(){
                return driver.getTitle().startsWith("Welcome");
            }



            public boolean clickOnLogin(LoginPage loginPage){
                loginLink1.click();
                return loginPage.checkCorrectPage();
            }

            public boolean clickOnFrames(LoginPage loginPage){
                gotoFrames.click();
                return loginPage.checkCorrectPage();
            }

}
