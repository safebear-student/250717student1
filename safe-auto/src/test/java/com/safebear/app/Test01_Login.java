package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/*this test will return a True statement
* on step 1
*/


public class Test01_Login extends BaseTest{

    @Test
    public void testLogin(){
        //step 1 Confirm we're on the welcome page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 click on the login link and the login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
//comment.